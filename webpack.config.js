const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extract = new ExtractTextPlugin('stylesheets/[name]-one.css');

module.exports = {
    entry: './main.js',
    output: {
        filename: 'bundle.js'
    },
    resolve: {
      modules: [
        'node_modules'
      ]
    },
    module: {
      loaders: [
            // Optionally extract less files
            // or any other compile-to-css language
            {
                test: /\.sass$/,
                loader: extract.extract(["css-loader", "sass-loader" ])
            },
            // Extract css files
                {
                    test: /\.css$/,
                    loader: extract.extract(["css-loader"])
                },
            {
          test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' //for handling Boostrap fonts
        }
      ]
    },
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
      extract
    ]
}
